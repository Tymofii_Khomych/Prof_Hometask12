﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // volatile (Справочник по C#)
            // https://learn.microsoft.com/ru-ru/dotnet/csharp/language-reference/keywords/volatile

            /*
             * Ключевое слово volatile означает, что поле может изменить несколько потоков, выполняемых одновременно. Компилятор, среда 
             * выполнения или даже аппаратное обеспечение могут изменять порядок операций чтения и записи в расположения в памяти для 
             * повышения производительности. Поля, которые объявлены volatile, исключаются из некоторых типов оптимизации. Нет никакой 
             * гарантии единого общего порядка временных записей во всех потоках выполнения. Дополнительные сведения см. в описании класса Volatile.
             * 
             * Ключевое слово volatile может применяться к полям следующих типов:
             *      -Ссылочные типы.
             *      -Типы указателей (в небезопасном контексте). Несмотря на то, что сам указатель может быть изменяемым, объект, на 
             *      который он указывает, должен быть постоянным. Другими словами, объявить указатель на изменяемый объект невозможно.
             *      -Простые типы, например sbyte, byte, short, ushort, int, uint, char, float и bool.
             *      -Тип enum с одним из следующих базовых типов: byte, sbyte, short, ushort, int или uint.
             *      -Параметры универсального типа называются ссылочными типами.
             *      -IntPtr и UIntPtr.
             */


            // Mutex Класс
            // https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.mutex?view=net-7.0

            /*
             * Если два или более потоков должны получить доступ к общему ресурсу одновременно, системе требуется механизм синхронизации, чтобы 
             * обеспечить использование ресурса только одним потоком. Mutex — это примитив синхронизации, предоставляющий монопольный доступ к 
             * общему ресурсу только одному потоку. Если поток получает мьютекс, второй поток, который хочет получить этот мьютекс, 
             * приостанавливается до тех пор, пока первый поток не выпустит мьютекс.
             */

            // Semaphore Класс
            // https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.semaphore?view=net-7.0

            /*
             * Используйте класс для Semaphore управления доступом к пулу ресурсов. Потоки входят в семафор, вызывая WaitOne метод , 
             * который наследуется от WaitHandle класса , и освобождают семафор путем вызова Release метода
             * 
             * Количество семафоров уменьшается каждый раз, когда поток входит в семафор, и увеличивается, когда поток освобождает семафор. 
             * Если счетчик равен нулю, последующие запросы блокируются до тех пор, пока другие потоки не отпустит семафор. Когда все потоки 
             * отпустили семафор, счетчик будет иметь максимальное значение, указанное при создании семафора.
             */
        }
    }
}
