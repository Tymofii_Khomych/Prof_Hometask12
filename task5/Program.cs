﻿using System;
using System.IO;
using System.Threading;

class Program
{
    static Semaphore semaphore;
    static string logFilePath = "access.log";

    static void Main()
    {
        semaphore = new Semaphore(1, 1);

        for (int i = 1; i <= 5; i++)
        {
            Thread thread = new Thread(DoWork);
            thread.Name = "Thread " + i;
            thread.Start();
        }

        Console.ReadLine();
    }

    static void DoWork()
    {
        Console.WriteLine(Thread.CurrentThread.Name + " is waiting for access.");

        semaphore.WaitOne();

        Console.WriteLine(Thread.CurrentThread.Name + " has gained access.");

        string logMessage = $"{DateTime.Now} - {Thread.CurrentThread.Name} has gained access.";
        WriteToLog(logMessage);

        Thread.Sleep(2000);

        semaphore.Release();

        Console.WriteLine(Thread.CurrentThread.Name + " has released access.");

        logMessage = $"{DateTime.Now} - {Thread.CurrentThread.Name} has released access.";
        WriteToLog(logMessage);
    }

    static void WriteToLog(string message)
    {
        using (StreamWriter writer = File.AppendText(logFilePath))
        {
            writer.WriteLine(message);
        }
    }
}
