﻿using System.Threading;
using System;

class Program
{
    static AutoResetEvent auto = new AutoResetEvent(false);

    static void Main()
    {
        Console.WriteLine("Початок програми.");

        new Thread(WaitForSignal).Start();

        Thread.Sleep(3000);
        Console.WriteLine("Відправлення сигналу.");
        auto.Set(); 

        auto.WaitOne();

        Console.WriteLine("Кінець програми.");
    }

    static void WaitForSignal()
    {
        Console.WriteLine("Потік очікує сигналу.");
        auto.WaitOne(); 
        Console.WriteLine("Потік отримав сигнал.");
        auto.Set(); 
    }
}
