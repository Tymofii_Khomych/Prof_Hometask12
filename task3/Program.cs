﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static Mutex mutex = new Mutex(false, "MyMutex"); 

        static void Main()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Thread[] threads = new Thread[5];

            for (int i = 0; i < 5; i++)
            {
                threads[i] = new Thread(Function);
                threads[i].Name = i.ToString();
                Thread.Sleep(500);
                threads[i].Start();
            }

            Console.ReadKey();
        }

        static void Function()
        {
            mutex.WaitOne();

            Console.WriteLine("Потік {0} зайшов у захищену область.", Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.WriteLine("Потік {0} покинув захищену область.\n", Thread.CurrentThread.Name);

            mutex.ReleaseMutex();
        }
    }
}
